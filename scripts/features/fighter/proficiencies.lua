
local feature = {}

local simple_weapons = {
  ["Handaxe"] = true
}

local martial_weapons = {
  ["Maul"] = true
}

function feature.has_proficiency(name)
  return simple_weapons[name] or martial_weapons[name]
end

return feature

