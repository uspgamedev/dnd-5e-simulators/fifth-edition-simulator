
local weapons = {}

weapons["maul"] = function ()
  return Weapon.new(
    "Maul",
    DamageFormula(
      2*d6,
      DamageType.BLUDGEONING
    ),
    WeaponType.MeleeMartial,
    Weight(10,1),
    10*100
  )
end

return weapons

