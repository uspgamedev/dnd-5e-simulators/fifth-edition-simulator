
local armors = {}

armors["plate"] = function ()
  return Armor.new(
    "Plate Armor",
    18,
    ArmorType.Heavy,
    Weight(65,1),
    1500*100
  )
end

armors["shield"] = function ()
  return Armor.new(
    "Shield",
    2,
    ArmorType.Shield,
    Weight(6,1),
    10*100
  )
end

return armors

