
local abilities = { 'str', 'dex', 'con', 'int', 'wis', 'cha' }

local ability_effects = {}

for i=1,2 do
  for _,abname in ipairs(abilities) do
    local name = abname .. '+' .. i
    local uppername = abname:upper()
    ability_effects[name] = function ()
      return AbilityScoreChange.new(AbilityType[uppername], i)
    end
  end
end

return ability_effects

