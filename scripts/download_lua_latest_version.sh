#!/bin/bash

VERSION=`curl -s https://www.lua.org/ftp/ | grep -o -m1 '[0-9]\.[0-9]\.[0-9]' | head -n1`
cd src; wget https://www.lua.org/ftp/lua-$VERSION.tar.gz; \
tar -xf lua-$VERSION.tar.gz; rm lua-$VERSION.tar.gz; mv lua-$VERSION lua
