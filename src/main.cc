
#include "armor.h"
#include "character.h"
#include "character_class.h"
#include "combat.h"
#include "config.h"
#include "damage.h"
#include "race.h"
#include "strategy.h"
#include "team.h"
#include "weapon.h"

#include "effects/ability_score_change.h"
#include "effects/hit_dice.h"
#include "effects/proficiency.h"

#include "utils/database.h"

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

const string PATH(SIMULATOR_SCRIPTS_PATH);

int main () {
    using namespace fes;

    // Equipments
    Weapon *maul = DB->get<Weapon>("maul");
    if (maul == nullptr) throw;
    Armor *plate_armor = DB->get<Armor>("plate");
    if (plate_armor == nullptr) throw;
    Armor *shield = DB->get<Armor>("shield");
    if (shield == nullptr) throw;
    ImprovisedWeapon shield_as_improvised_wpn(shield, DamageType::BLUDGEONING, true);

    // Race
    Effect *test = DB->get<Effect>("con+2");
    //FixedAbilityScoreChange ability_effect(Ability::Type::CON, 2);
    RacialTrait dwarf_trait({test});
    Race dwarf(vector<RacialTrait*>{&dwarf_trait});

    // Class
    //
    Effect *proficiency = DB->get<Effect>("proficiency-with-all-weapons");
    if (proficiency == nullptr) throw;
    ClassFeature fighter_feature1(vector<const Effect*>{ proficiency });

    Effect *hit_dice = DB->get<Effect>("hitdice-d10");
    if (hit_dice == nullptr) throw;
    ClassFeature fighter_feature2(vector<const Effect*>{ hit_dice });

    CharacterClass::FeatureTable fighter_features = {
        { &fighter_feature1, &fighter_feature2 },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
    };
    CharacterClass fighter_class(fighter_features);
    dynamic_cast<HitDice*>(hit_dice)->set_original_class(&fighter_class);

    // Character
    Character character("Bobdude", &dwarf, Strategy::default_strategy(),
                        15u, 13u, 14u, 8u, 10u, 12u);
    Character character2("Bobfella", &dwarf, Strategy::default_strategy());

    // Stuff
    character << &fighter_class << &fighter_class << &fighter_class;
    character << maul << plate_armor << shield;
    character.equip(maul, plate_armor, shield);
    character2 << &fighter_class;
    character2 << maul << plate_armor << shield;
    character2.equip(maul, plate_armor, shield);
    cout << "===" << endl;
    cout << character;
    cout << "===" << endl;
    cout << character2;
    cout << "===" << endl;

    Combat combat;
    Team dudes, fellas;
    dudes << &character;
    fellas << &character2;
    combat.addTeamOne(&dudes);
    combat.addTeamTwo(&fellas);
    combat.fight();

    return 0;
}

