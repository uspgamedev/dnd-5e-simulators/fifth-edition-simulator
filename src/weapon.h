
#ifndef FES_WEAPON_H_
#define FES_WEAPON_H_

#include "equipment.h"
#include "damage.h"
#include "dice.h"

namespace fes {

class Weapon : public Equipment {
  public:
    enum class Type {
        MeleeSimple, MeleeMartial, RangedSimple, RangedMartial
    };
    Weapon(const std::string& the_name, DamageFormula damage, Type the_type, Weight the_weight,
           uint64_t the_cost);
    uint64_t roll() const;
    DamageFormula damage() const { return damage_; }
    Type type() const { return type_; }
    bool is_ranged() const { return type_ == Type::RangedSimple || type_ == Type::RangedMartial; }
    bool is_melee() const { return !is_ranged(); }
    bool is_martial() const { return type_ == Type::MeleeMartial || type_ == Type::RangedMartial; }
    bool is_simple() const { return !is_martial(); }
  private:
    DamageFormula damage_;
    Type type_;
};

class ImprovisedWeapon : public Weapon {
  public:
    ImprovisedWeapon(const Equipment* equipment, DamageType damage_type, bool throwable)
        : Weapon(equipment->name(), DamageFormula{ d4, damage_type }, Type::MeleeSimple,
          equipment->weight(), equipment->cost()), equipment_(equipment), throwable_(throwable) {}
  private:
    const Equipment *equipment_;
    bool throwable_;
};

std::ostream& operator <<(std::ostream &out, const Weapon& weapon);

} // namespace fes

#endif // FES_WEAPON_H_

