
#ifndef FES_PROFICIENCY_H_
#define FES_PROFICIENCY_H_

#include "effects/effect.h"

#include "armor.h"
#include "weapon.h"

namespace fes {

template <class T>
class Proficiency : virtual public Effect {
  public:
    virtual ~Proficiency() {}
    virtual bool with(const T *thing) const = 0;
    std::type_index type_index() const { return typeid(Proficiency<T>); }
};

using WeaponProficiency = Proficiency<Weapon>;
using ArmorProficiency = Proficiency<Armor>;

template <class T>
class SingleProficiency : virtual public Proficiency<T> {
  public:
    SingleProficiency(const T* the_thing) : thing_(the_thing) {}
    bool with(const T *thing) const override { return thing == thing_; }
  private:
    const T* thing_;
};

using SingleWeaponProficiency = SingleProficiency<Weapon>;
using SingleArmorProficiency = SingleProficiency<Armor>;

template <class T>
class TypeProficiency : virtual public Proficiency<T> {
  public:
    using Type = typename T::Type;
    TypeProficiency(const std::vector<Type>& types)
        : types_(types) {}
    bool with(const T *thing) const override {
        for (Type type : types_)
            if (thing->type() == type)
                return true;
        return false;
    }
  private:
    std::vector<Type> types_;
};

using WeaponTypeProficiency = TypeProficiency<Weapon>;
using ArmorTypeProficiency = TypeProficiency<Armor>;

} // namespace fes

#endif // FES_PROFICIENCY_H_

