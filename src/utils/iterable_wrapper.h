
#ifndef FES_ITERABLE_WRAPPER_H_
#define FES_ITERABLE_WRAPPER_H_

namespace fes {

template <class C>
class IterableWrapper {
  public:
    IterableWrapper(const C& container) : container_(container) {}
    auto begin() const { return container_.begin(); }
    auto end() const { return container_.end(); }
  private:
    const C& container_;
};

} // namespace fes

#endif // FES_ITERABLE_WRAPPER_H_

