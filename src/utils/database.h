
#ifndef FES_DATABASE_H_
#define FES_DATABASE_H_

#include "ability.h"
#include "config.h"

#include "weapon.h"
#include "armor.h"

#include "effects/ability_score_change.h"
#include "effects/hit_dice.h"
#include "effects/proficiency.h"

#include <sol2/sol.hpp>

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;
using std::vector;

namespace sol {

namespace stack {

using fes::Weapon;

template <>
struct checker<vector<Weapon::Type>> {
    template <typename Handler>
    static bool check(lua_State* L, int index, Handler&& handler, record& tracking) {
        tracking.use(1);
        return lua_istable(L, index);
    }
};

template <>
struct getter<vector<Weapon::Type>> {
    static vector<Weapon::Type> get(lua_State *L, int index, record& tracking) {
        tracking.use(1);
        vector<Weapon::Type> result;
        lua_len(L, index);
        int len = lua_tointeger(L, -1);
        lua_pop(L, 1);
        for (int i = 0; i < len; i++) {
            lua_pushinteger(L, i);
            lua_gettable(L, index);
            result.push_back(getter<Weapon::Type>::get(L, -1, tracking));
            lua_pop(L, 1);
        }
        return result;
    }
};

} // namespace stack

} // namespace sol

namespace fes {

namespace {

const string PATH(SIMULATOR_SCRIPTS_PATH);

class Database {
  public:
    Database();
    template <class T>
    T* get(const string& name);
  private:
    sol::state lua;
};

} // unnamed namespace

unique_ptr<Database> DB(new Database);

Database::Database() {
    lua.open_libraries();
    lua.new_enum("AbilityType",
        "STR", Ability::Type::STR,
        "DEX", Ability::Type::DEX,
        "CON", Ability::Type::CON,
        "INT", Ability::Type::INT,
        "WIS", Ability::Type::WIS,
        "CHA", Ability::Type::CHA
    );
    lua.new_enum("WeaponType",
        "MeleeSimple", Weapon::Type::MeleeSimple,
        "MeleeMartial", Weapon::Type::MeleeMartial,
        "RangedSimple", Weapon::Type::RangedSimple,
        "RangedMartial", Weapon::Type::RangedMartial
    );
    lua.new_enum("ArmorType",
        "Light", Armor::Type::Light,
        "Medium", Armor::Type::Medium,
        "Heavy", Armor::Type::Heavy,
        "Shield", Armor::Type::Shield
    );
    lua.new_enum("DamageType",
        "ACID", DamageType::ACID,
        "BLUDGEONING", DamageType::BLUDGEONING,
        "COLD", DamageType::COLD,
        "FIRE", DamageType::FIRE,
        "FORCE", DamageType::FORCE,
        "LIGHTNING", DamageType::LIGHTNING,
        "NECROTIC", DamageType::NECROTIC,
        "PIERCING", DamageType::PIERCING,
        "POISON", DamageType::POISON,
        "PSYCHIC", DamageType::PSYCHIC,
        "RADIANT", DamageType::RADIANT,
        "SLASHING", DamageType::SLASHING,
        "THUNDER", DamageType::THUNDER
    );
    lua.new_usertype<Dice>("Dice",
        sol::meta_function::multiplication, [](size_t n, const Dice& dice) { return n*dice; }
    );
    lua["DamageFormula"] = [] (Dice dice, DamageType type) {
        return DamageFormula{ dice, type };
    };
    lua["Weight"] = [] (size_t n, size_t d) {
        return Weight{ n, d };
    };
    lua.new_usertype<Weapon>("Weapon",
        sol::constructors<Weapon(const string&, DamageFormula, Weapon::Type, Weight, uint64_t)>()
    );
    lua.new_usertype<Armor>("Armor",
        sol::constructors<Weapon(const string&, size_t, Armor::Type, Weight, uint64_t)>()
    );
    lua["d4"] = d4;
    lua["d6"] = d6;
    lua["d8"] = d8;
    lua["d10"] = d10;
    lua["d12"] = d12;
    lua["d20"] = d20;
    lua.new_usertype<FixedAbilityScoreChange>("AbilityScoreChange",
        sol::constructors<FixedAbilityScoreChange(Ability::Type, int)>()
    );
    lua.new_usertype<WeaponTypeProficiency>("WeaponTypeProficiency",
        sol::constructors<WeaponTypeProficiency(const vector<Weapon::Type>&)>()
    );
    lua.new_usertype<HitDice>("HitDice",
        sol::constructors<HitDice(size_t)>()
    );
    // Set scripts folder as the sole Lua path
    lua.script(string("package.path = '") + PATH + "/?.lua'");
    // This function is used to debug loaded scripts
    lua.script("function check_err(msg) print(msg) end");
    // Load all effects from core script
    sol::load_result script = lua.load_file(PATH + "/all.lua");
    if (script.valid()) {
        sol::protected_function load(static_cast<sol::protected_function>(script));
        // Set the error handler to capture anything that goes wrong
        load.error_handler = lua["check_err"];
        // Execute the creator function
        load();
    } else {
        cout << "Could not load script" << endl;
        throw;
    }
}

template <class T>
T* Database::get(const string& name) {
    sol::function factory = lua[T::TABLE_NAME][name];
    return factory();
}

} // namespace


#endif // FES_DATABASE_H_


