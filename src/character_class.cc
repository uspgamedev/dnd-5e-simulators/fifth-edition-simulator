
#include "character_class.h"

#include "effects/proficiency.h"

using std::string;
using std::vector;

namespace fes {

CharacterClass::CharacterClass(const FeatureTable& features) : features_(features) {}

} // namespace fes

