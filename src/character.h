
#ifndef FES_CHARACTER_H_
#define FES_CHARACTER_H_

#include "ability.h"
#include "combat.h"
#include "dice.h"
#include "equipment.h"
#include "effects/proficiency.h"

#include <algorithm>
#include <string>
#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include <cstdlib>
#include <cstdint>

namespace fes {

class Armor;
class CharacterClass;
class Effect;
class Race;
class Strategy;
class Weapon;
struct Damage;

class Character {
  public:
    Character(const std::string &the_name, const Race *the_race, const Strategy* initial_strategy,
              uint64_t STR, uint64_t DEX, uint64_t CON, uint64_t INT, uint64_t WIS, uint64_t CHA);
    Character(const std::string &the_name, const Race *the_race, const Strategy* initial_strategy);

    std::string name() const { return name_; }
    size_t hit_point_maximum() const {
        return hit_point_maximum_ + total_level() * modifier(Ability::Type::CON);
    }
    size_t hit_points() const {
        return hit_point_maximum() - damage_;
    }
    bool unconscious() const {
        return hit_points() == 0u;
    }
    bool conscious() const {
        return !unconscious();
    }

    // Getters
    int64_t modifier(Ability::Type which_type) const;
    int64_t modifier(size_t which_type) const {
        return modifier(static_cast<Ability::Type>(which_type));
    }
    Ability ability(Ability::Type which_type) const {
        return ability(static_cast<size_t>(which_type));
    }
    Ability ability(size_t which_type) const {
        return abilities_[which_type];
    }
    size_t total_level() const {
        return class_levels_.total_level();
    }
    size_t proficiency() const {
        return (total_level() - 1)/4 + 2;
    }
    size_t critical_hit_threshold() const { return 20u; }
    const Equipment* findFirstEquipped(const std::string& slot) const;
    const Weapon* findFirstEquippedWeapon() const;
    const Race* race() const { return race_; }
    template <class EffectType>
    std::vector<const EffectType*> effects() const {
        using namespace std;
        auto it = effect_groups_.find(typeid(EffectType));
        if (it != effect_groups_.end()) {
            vector<const Effect*> uncast_effects = it->second;
            vector<const EffectType*> cast_effects(uncast_effects.size(), nullptr);
            transform(uncast_effects.begin(), uncast_effects.end(), cast_effects.begin(),
                      [] (const Effect* effect) {
                          const EffectType* cast_effect = dynamic_cast<const EffectType*>(effect);
                          return cast_effect;
                      });
            return cast_effects;
        }
        return vector<const EffectType*>();
    }

    // Setters, Inserters, etc.
    Character& operator <<(const Equipment* new_equipment) {
        inventory_.insert(new_equipment, 1u); return *this;
    }
    Character& operator <<(const CharacterClass* increased_class);
    void receiveEquipmentAmount(const Equipment* new_equipment, size_t amount = 1) {
        inventory_.insert(new_equipment, amount);
    }
    size_t armor_class() const;
    void equip(Armor* armor);
    void equip(Weapon* weapon);
    template <class T, class... Args>
    void equip(T* equipment, Args... args);
    void unequip(Armor* armor);
    void unequip(Weapon* weapon);
    template <class T, class... Args>
    void unequip(T* equipment, Args... args);
    template <class T>
    bool has_proficiency(const T* thing) const {
        for (const Proficiency<T>* proficiency : effects<Proficiency<T>>())
            if (proficiency->with(thing)) return true;
        return false;
    }
    // Combat-related
    std::tuple<size_t, Damage> rollAttack(const Weapon *weapon_used) const;
    void takeDamage(const Damage& damage);
    std::unordered_set<Dice> hit_dice() const { return class_levels_.hit_dice(); }
    bool attack(Character* target, const Weapon *weapon_used);
    int rollInitiative() const;
    Combat::Action* act(Combat* combat);

  private:

    friend std::ostream& operator <<(std::ostream &out, const Character& character);
    friend class ClassLevel;

    class Inventory {
      public:
        Weight total_weight() const;
        void insert(const Equipment* new_equipment, size_t amount);
      private:
        std::unordered_map<const Equipment*, size_t> items_;
    };

    class ClassLevel {
      public:
        ClassLevel(Character *owner) : owner_(owner) {}
        // This isn't const because of unordered_map's [] operator
        size_t levels_in_class(const CharacterClass* which_class);
        size_t total_level() const;
        void increaseLevel(const CharacterClass* which_class);
        std::unordered_set<Dice> hit_dice() const { return hit_dice_; }
      private:
        Character *const owner_;
        std::unordered_map<const CharacterClass*, size_t> levels_;
        std::unordered_set<Dice> hit_dice_;
    };

    void unequipFromSlot(const std::string& equipment_slot, const Equipment* equipment);

    std::string name_;
    size_t hit_point_maximum_;
    size_t damage_;
    const Race *race_;
    Ability abilities_[Ability::NUM];
    Inventory inventory_;
    std::unordered_multimap<std::string, const Equipment*> equipped_;
    ClassLevel class_levels_;
    const Strategy *current_strategy_;
    std::unordered_map<std::type_index, std::vector<const Effect*>> effect_groups_;

};

template <class T, class... Args>
inline void Character::equip(T* equipment, Args... args) {
   equip(equipment);
   equip(args...);
}

template <class T, class... Args>
inline void Character::unequip(T* equipment, Args... args) {
   unequip(equipment);
   unequip(args...);
}

std::ostream& operator <<(std::ostream& out, const Character& character);
inline std::ostream& operator <<(std::ostream& out, const Character* character) {
    return (out << *character);
}

} // namespace fes

#endif // FES_CHARACTER_H_

