
#ifndef FES_COMBAT_H_
#define FES_COMBAT_H_

#include <vector>

#include <cstdint>

namespace fes {

class Character;
class Team;
class Weapon;

class Combat {
  public:
    class Action;
    class IdleAction;
    class AttackAction;
    enum class TeamName : bool {
      TEAM_ONE, TEAM_TWO
    };
    void addTeamOne(Team *team) { addTeam(TeamName::TEAM_ONE, team); }
    void addTeamTwo(Team *team) { addTeam(TeamName::TEAM_TWO, team); }
    Team* team1() const { return team1_; }
    Team* team2() const { return team2_; }
    Team* teamOf(Character *character) const;
    Team* enemyTeamOf(Character *character) const;
    void fight();
  private:
    void rollInitiatives();
    bool end() const;
    void attack(const std::vector<Character*>& involved, const Weapon* weapon);
    void addTeam(TeamName name, Team *team);
    Team *team1_;
    Team *team2_;
    std::vector<Character*> order_;
};

class Combat::Action {
  protected:
    friend class Combat;
    Action() {}
    virtual ~Action() {}
    virtual void execute() = 0;
};

class Combat::IdleAction : public Combat::Action {
  public:
    IdleAction() {}
  private:
    void execute() override {}
};

class Combat::AttackAction : public Combat::Action {
  public:
    AttackAction(Character* attacker, Character* defender, const Weapon* weapon)
        : attacker_(attacker), defender_(defender), weapon_(weapon) {}
  private:
    void execute() override;
    Character *attacker_;
    Character *defender_;
    const Weapon *weapon_;
};

} // namespace fes

#endif // FES_COMBAT_H_

