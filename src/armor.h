
#ifndef FES_ARMOR_H_
#define FES_ARMOR_H_

#include "equipment.h"

namespace fes {

class Armor : public Equipment {
  public:
    enum class Type {
        Light, Medium, Heavy, Shield
    };
    Armor(const std::string& the_name, size_t armor_class, Type the_type, Weight the_weight,
          uint64_t the_cost);
    Type type() const { return type_; }
    size_t armor_class() const { return armor_class_; }
    bool is_light() const { return type_ == Type::Light; }
    bool is_medium() const { return type_ == Type::Medium; }
    bool is_heavy() const { return type_ == Type::Heavy; }
    bool is_shield() const { return type_ == Type::Shield; }
  private:
    size_t armor_class_;
    Type type_;
};

} // namespace fes

#endif // FES_WEAPON_H_

